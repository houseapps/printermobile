import React, { Component } from 'react';
import {
  Alert,
  Platform,
  StyleSheet,
  Text,
  View,
  Button
} from 'react-native';
import { BleManager } from 'react-native-ble-plx';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};

const targetDeviceId = 'D4:36:39:CC:F3:5E'
export default class App extends Component<Props> {

  constructor(props) {
    super(props);
    this.state = {
      scanning: false,
      findBeacons: [],
      tableHead: ['ID', 'NAME', 'RSSI', 'SERVICE UUID'],
      tableData: [],
        service: '',
    };
    this.manager = new BleManager();
    this.device = null
    this.ring             = this.ring.bind(this)
    this.cancelConnection = this.cancelConnection.bind(this)
    this.checkConnection  = this.checkConnection.bind(this)
    this.connect          = this.connect.bind(this)
    this.checkCharacteristicsForDevice = this.checkCharacteristicsForDevice.bind(this)
  }

  componentWillMount() {
    const subscription = this.manager.onStateChange((state) => {
      if (state === 'PoweredOn') {
        console.log("startScan")
        this.scanAndConnect();
        subscription.remove();
      }
    }, true);
  }

  scanAndConnect() {
    this.manager.startDeviceScan(null, null, (error, device) => {
      if (error) {
        console.log(`SCAN_ERROR: ${error}`)
        // Handle error (scanning will be stopped automatically)
        return
      }
      // Check if it is a device you are looking for based on advertisement data
      // or other criteria.
      // console.log(JSON.stringify(device))
      const row =  [device.id, device.name, device.rssi, device.serviceUUIDs]

      if(device.id === targetDeviceId && this.device === null) {


        // this.connectTo(device)
        this.setState(prevState => (
          {
            tableData: [row, ...prevState.tableData,]
          }
        ))
      }

      if (device.name === 'TI BLE Sensor Tag' ||
        device.name === 'Printer_F35E') {
          if(device.serviceUUIDs) {
            this.device = device
            console.log('device:', device);
            this.manager.stopDeviceScan();
          }

        // Stop scanning as it's not necessary if you are scanning for one device.

        // Proceed with connection.
      }
    });
  }


  ring(){
    this.manager.writeCharacteristicWithResponseForDevice(
      this.device.id,
      '000018f0-0000-1000-8000-00805f9b34fb',
      'e7810a71-73ae-499d-8c15-faa9aef0c3f2',
      'YWJjZGVmZw=='
    ).then((res) => {
      console.log(`WRITE RES ${res}`)
    }).catch((error) => {
      console.log(`WRITE ERROR ${error}`)
    })
  }

  connect(){
    console.log("try to connect")

    this.device.connect()
      .then((dev) => {
        console.log(dev);
        console.log("try to connect", this.device.discoverAllServicesAndCharacteristics())
        this.device.discoverAllServicesAndCharacteristics().then((resp) => {
          console.log('discoverAllServicesAndCharacteristics resp', resp);
        }).catch((err) => {
          console.log('discoverAllServicesAndCharacteristics err', err);
        })
        return this.device.discoverAllServicesAndCharacteristics();
      })
      .then((dev) => {
        console.log("success to connect", dev)
          // this.device = dev;
      })
      .catch((error) => {
        console.log("connect error:" + error)
      });
    // this.manager.connectToDevice(this.device.id).then((response) => {
    //   return response.discoverAllServicesAndCharacteristics()
    // }).catch((error) => {
    //   console.log('connectToDevice error', error);
    // })
  }

  cancelConnection(){
    this.device.cancelConnection()
      .then((res) => {
        console.log("connection cancel")
        console.log(res)
      })
      .catch((err) => {
        console.log("connection cancel error:" + err)
      })
  }

  checkConnection(){
      this.manager.servicesForDevice(this.device.id).then((dev) => {
          console.log("services ", dev)
          this.device.characteristicsForService(dev[0].uuid).then((read) => {
            console.log('characteristicsForService0', read)
          })
          this.device.characteristicsForService(dev[1].uuid).then((read) => {
            console.log('characteristicsForService1', read)
          })
          this.device.characteristicsForService(dev[2].uuid).then((read) => {
            console.log('characteristicsForService2', read)
          })
      })
          .catch((error) => {
              console.log("services error:" + error)
          });
      this.manager.requestMTUForDevice(this.device.id, 120)
        .then((res) => {
          console.log("requestMTUForDevice res", res);
        })
        .catch((err) => {
          console.log("requestMTUForDevice error", err)
        })
      this.device.services().then((obj) => {
          console.log('OBJ', obj);
          obj[0].characteristics().then((data) => {
            obj[0].monitorCharacteristic(
              data[0].uuid,
              (error, characteristic) =>
              {
                console.log('CCCCC error', error);
                console.log('CCCCC characteristic', characteristic);
              },
            )
 
            console.log('AAAAAA', data);
            this.device.monitorCharacteristicForService(
              data[1].serviceUUID,
              data[1].uuid,
              (error, characteristic) =>
                {
                  console.log('BBBBB error', error);
                  console.log('BBBBB characteristic', characteristic);
                },
            );

            this.device.readCharacteristicForService(data[0].serviceUUID,data[0].uuid).then((data) => {
 

              data.writeWithResponse('dXRrdWNhbg==').then((resp) => {
                console.log('writeWithResponse resp', resp)
              }).catch((err) => {
                console.log('writeWithResponse err', err)
              })
              
              console.log('data', data);
              console.log('payload', this.device.id,'-----',data.serviceUUID,'----',data.uuid,'----','dXRrdWNhbg==')
              this.device.writeCharacteristicWithResponseForService(data.serviceUUID,data.uuid,'dXRrdWNhbg==').then((dev) => {
                  console.log("success to write", dev)
              })
                  .catch((error) => {
                      console.log("write error:" + error)
                  });

          }).catch((err) => {
              console.log('err', err);
          })

          });
          console.log('utku0', obj[0].characteristics());
          console.log('utku1', obj[1].characteristics());
          console.log('utku2', obj[2].characteristics());

          console.log('TEST', obj[1].readCharacteristic('00002a05-0000-1000-8000-00805f9b34fb'))
          

          
      });


      this.device.isConnected()
      .then((res) => {
        console.log("connection check:" + res)
      })
      .catch((err) => {
        console.log("connection check:" + err)
      })
  }
  checkCharacteristicsForDevice() {
    this.manager.characteristicsForDevice(
      this.device.id,
      'e7810a71-73ae-499d-8c15-faa9aef0c3f2'
    ).then((res) => {
      console.log('characteristicsForDevice', res)
    }).catch((error) => {
      console.log('characteristicsForDevice', error)
    })
    this.manager.characteristicsForDevice(
      this.device.id,
      '000018f0-0000-1000-8000-00805f9b34fb'
    ).then((res) => {
      console.log('characteristicsForDevice 2', res)
    }).catch((error) => {
      console.log('characteristicsForDevice 2', error)
    })
  }

  render() {
    return (
      <View style={styles.container}>
        <Button title="Ring" onPress={this.ring}></Button>
        <Button title="Connect" onPress={this.connect}></Button>
        <Button title="Cancel Connection" onPress={this.cancelConnection}></Button>
        <Button title="Check Connection" onPress={this.checkConnection}></Button>
        <Button title="Check CharacteristicsForDevice" onPress={this.checkCharacteristicsForDevice}></Button>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: { flex: 1, padding: 16, paddingTop: 30, backgroundColor: '#fff' },
  head: { height: 40, backgroundColor: '#f1f8ff' },
  text: { margin: 6 }
});
